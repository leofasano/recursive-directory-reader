use std::fs;
// use std::str::rfind;

fn main() {
    let dir_path: &str = "./targetDir";
    let file_extention: &str = ".txt";
    let line_count = recursivly_get_line_count_in_dir(dir_path, file_extention);
    println!("The directory {} has a total of {} lines in all its {} files", dir_path, line_count, file_extention)
}

fn recursivly_get_line_count_in_dir(dir_path: &str, file_extension: &str) -> usize {
    let mut total_lines: usize = 0;

    for entry in fs::read_dir(dir_path).expect("I told you this directory exists") {
        let entry = entry.expect("I couldn't read something inside the directory");
        let path = entry.path();

        if path.is_dir() {
            let directory_path = &path.into_os_string().into_string().unwrap()[..];
            total_lines = total_lines + recursivly_get_line_count_in_dir(directory_path, file_extension);
        }
        else {
            let file_path = &path.into_os_string().into_string().unwrap()[..];
            let file_has_target_ext: bool = file_path.rfind(file_extension).unwrap_or(0) != 0;
            if file_has_target_ext {
                let contents = fs::read_to_string(file_path)
                    .expect("Something went wrong reading the file");
                let line_count = contents.matches("\n").count() + 1;
                total_lines = total_lines + line_count;
            }
        }
    }

    total_lines
}
