# Recursive directory reader

## Getting started

Firstly you need to [install rust](https://www.rust-lang.org/tools/install).

### Clone the project
```bash
# with SSH
git clone git@gitlab.com:leofasano/recursive-directory-reader.git

# with HTTP
git clone https://gitlab.com/leofasano/recursive-directory-reader.git
```

### Build the project

```bash
cargo build
```

### Run the project

```bash
cargo run
```

## Update the inputs

Go into _**./src/main.rs**_ and update the argument of the variables **dir_path** and **file_extention**.

```rust
// main.rs file
use std::fs;
// use std::str::rfind;

fn main() {
    let dir_path: &str = "./targetDir"; // <---- Input variable 
    let file_extention: &str = ".txt"; // <---- Input variable 
    
    let line_count = recursivly_get_line_count_in_dir(dir_path, file_extention);
    println!("The directory {} has a total of {} lines in all its {} files", dir_path, line_count, file_extention)
}

fn recursivly_get_line_count_in_dir(dir_path: &str, file_extension: &str) -> usize {
    [...]
}

```